<!DOCTYPE html>
<html>
<head>
    <link rel="shortcut icon" href="#" />
    <script type="text/javascript" src="./jquery/jquery-3.4.1.min.js"></script>
</head>
<body>
<div id="codets"></div>
    
<script>
// in: YYYYMMDDHHmm
// uit: unix time stamp
function parseDate(datumtijd)
{
	return Date.parse(
		datumtijd.substr(6,2)+'-'
		+ datumtijd.substr(4,2)+'-'
		+ datumtijd.substr(0,4)+' '
		+ datumtijd.substr(8,2)+':'
		+ datumtijd.substr(10,2)+'Z'
		)/1000;
}

// load custom wawa font ---------- does not work in IE! -----------
function loadFont()
{
    let wawafont = new FontFace("wawa", "url("+wawattf+"?v=2)");
    wawafont.load().then(
        font => {
            // if font loaded, make it available
            document.fonts.add(font);
            
        },
        error => {
            // font could not be loaded
            console.log(error);
        }
    );
}   

$(document).ready(function()
{
    loadFont();
    let t = JSON.parse(window.localStorage.getItem('xdata'));
    let x = t.map(parseDate);
    let y1 = JSON.parse(window.localStorage.getItem('ydata1'));
    let parameter1 = window.localStorage.getItem('parameter1');
    let tijden = '';
    
    for (i=0; i<x.length; i++){
        tijden += x[i];
    }
    $("#codets").html(tijden);
});
</script>

</body>
</html>
