// canvas
var canvas;
var context;

// graphics directory
const graphics_dir = './graphics/';

// afbeeldingen
const background_map = graphics_dir+'mapNL.svg';
var background = new Image();
const gg_orange_png = graphics_dir+'gg_orange.png';
const gg_red_png = graphics_dir+'gg_red.png';
const fg_orange_png = graphics_dir+'fg_orange.png';
const fg_red_png = graphics_dir+'fg_red.png';
const st_orange_png = graphics_dir+'st_orange.png';
const st_red_png = graphics_dir+'st_red.png';
const ts_png = graphics_dir+'ts.png';
const winter_png = graphics_dir+'winter.png';

// data
const last_nl = './lastnl.dat';
const data_json = './obs.json';
const nlstations = './stations_ned.json?random=2';
const txx_json = './txx.json';
const txn_json = './txn.json';
const tnx_json = './tnx.json';
const tnn_json = './tnn.json';
const ECrun_json = './EC_run.json';
const ECts_json = './EC_ts.json';

var stations = [];
var obs;
var lastplot;
var txx, txn, tnx, tnn;
var ECrun, ECts;

// wawa font
const wawattf = graphics_dir+'wawa.ttf';
const wawaFontSize = 36;
const minWawaFontSize = 24;

// value font
const valueFontSize = 14;
const minValueFontSize = 12;

// panelen
var panel_rows = 2;
var panel_cols = 3;
var panel_selected = [1, 1];
var panel_height;
var panel_width;
var panel_pars;
var cvs;    // foreground layer
var ctx;    // foreground layer
var cvsb;   // background layer
var ctxb;   // background layer

// parameter settings
var pars =  [['wind', 'gust_kn', 'ttd', 'twt10', 'txx_x'],
            ['cc', 'vis', 'wx', 'rg', 'qnh']];

// current time
var current_time;

// hoekpunten van volledige domein
const full_domain_lonn = 0.0;
const full_domain_lonx = 7.5;
const full_domain_latn = 50.0;
const full_domain_latx = 55.8;

// hoekpunten van volledige FIR
const fir_domain_lonn = 1.9;
const fir_domain_lonx = 7.5;
const fir_domain_latn = 50.7;
const fir_domain_latx = 55.8;

// hoekpunten uitsnede met alleen Nederland
const nl_domain_lonn = 3.3;
const nl_domain_lonx = 7.3;
const nl_domain_latn = 50.7;
const nl_domain_latx = 53.6;

// huidige domein
var cur_domain_lonn;
var cur_domain_lonx;
var cur_domain_latn;
var cur_domain_latx;

// hoekpunten huidige uitsnede in pixelcoördinaten
var lower_left_corner_x;
var lower_left_corner_y;
var upper_right_corner_x;
var upper_right_corner_y;

// scaling
var scaleX;
var scaleY;
var mapscale;
var scale_factor = 900;   // for scaling fonts / wind vanes

// LUT available parameters
// internal par name, full name, panel name, plot pars
const availableParameters = {
    'wind': [['Windvaantjes'], ['wind']],
    'gust_kn': [['Windstoten'], ['gust (kn)']],
    'Bft': [['Wind (Bft)'], ['wind (Bft)']],
    'gust_kmu': [['Windstoten (km/u)'], ['gust (km/u)']],
    'Sx1H': [['Max windstoot 1u'], ['Max windstoot 1u']],
    'Sx3H': [['Max windstoot 3u'], ['Max windstoot 3u']],
    'Sx6H': [['Max windstoot 6u'], ['Max windstoot 6u']],
    
    'ta': [['T2m'], ['T2m']],
    'td': [['Td2m'], ['Td2m']],
    'tgn': [['T10cm'], ['T10cm']],
    'tb': [['Tnattebol'], ['T2wb']],
    'ttd': [['T2m & Td2m'], ['T2m', 'Td2m']],
    'twt10': [['Tnattebol & T10cm'], ['Twb2m', 't10cm']],
    'tx': [['Tx afgelopen 10 min'], ['Tx 10 min']],
    'Tx6': [['Tx afgelopen 6 u'], ['Tx6']],
    'Tx12': [['Tx afgelopen 12 uur'], ['Tx12']],
    'Tx24': [['Tx afgelopen 24 uur'], ['Tx24']],
    'tn': [['Tn afgelopen 10 min'], ['Tn 10 min']],
    'Tn6': [['Tn afgelopen 6 uur'], ['Tn6']],
    'Tn12': [['Tn afgelopen 12 uur'], ['Tx12']],
    'Tn14': [['Tn afgelopen 14 uur'], ['Tn14']],
    
    'txx_j': [['Txx ooit gemeten'], ['Txx ooit']],
    'txx_s': [['Txx seizoen'], ['Txx seizoens']],
    'txx_m': [['Txx maand'], ['Txx maand']],
    'txx_x': [['Txx decade'], ['Txx decade']],
    'txx_d': [['Txx dag'], ['Txx dag']],
    
    'tnn_j': [['Tnn ooit gemeten'], ['Tnn ooit']],
    'tnn_s': [['Tnn seizoen'], ['Tnn seizoen']],
    'tnn_m': [['Tnn maand'], ['Tnn maand']],
    'tnn_x': [['Tnn decade'], ['Tnn decade']],
    'tnn_d': [['Tnn dag'], ['Tnn dag']],
    
    'nc': [['Totale bewolking'], ['Totale bewolking']],
    'cc': [['Wolkenbasis & 1e laag'], ['ceiling', 'layer1']],
    'h1': [['1e wolkenlaag'], ['layer1']],
    'h2': [['2e wolkenlaag'], ['layer2']],
    'h3': [['3e wolkenlaag'], ['layer3']],
    'lcl': [['(T2m-Td2m)*400'], ['LCL']],
    
    'vis': [['VIS & MOR'], ['VIS', 'MOR']],
    'visttd': [['MOR & T2m-Td2m'], ['MOR', 'T-Td']],
    'visgr': [['MOR & global rad'], ['MOR', 'Global rad']],
    'wx': [['Weer'], ['WaWa']],
    
    'rg': [['Rain gauge'], ['Rain gauge']],
    'dr': [['Neerslagduur'], ['Neerslagduur']],
    'R1H': [['Neerslag 1u'], ['Neerslag 1u']],
    'R6H': [['Neerslag 6u'], ['Neerslag 6u']],
    'R12H': [['Neerslag 12u'], ['Neerslag 12u']],
    'R24H': [['Neerslag 24u'], ['Neerslag 24u']],
    
    'qnh': [['Luchtdruk'], ['QNH']],
    'ptd': [['Druktendens'], ['Druktendens 3 uur']],
    'qg': [['Global radiation'], ['Global rad']],
    'ss': [['Sunshine duration'], ['Sunshine dur']],
    'rh': [['Relative humidity'], ['Rel hum']],

    'EC2t': [['EC - T2m'], ['EC - T2m']],
    'EC2d': [['EC - Td2m'], ['EC - Td2m']]     
}


// warning criteria
const windstoten_oranje = 54; // knopen
const windstoten_rood = 65; // knopen
const wolkenbasis_oranje = 1000; // voet
const wolkenbasis_rood = 500; //
const zicht_oranje = 1000; // meters
const zicht_rood = 200; // meters
const onweer_rood = [12, 90, 91, 92, 93, 94, 95, 96];
const wx_rood = [11, 45, 46, 47, 48, 54, 55, 56, 64, 65, 66, 67, 68, 70,
    71, 72, 73, 74, 75, 76, 77, 78, 85, 86, 87, 89];

// kleuren +  grenzen
const zicht_kleuren = ['red', 'orange', 'yellow', 'black'];
const zicht_grenzen = [0, 200, 1000, 5000];
const wolken_kleuren = ['red', 'orange', 'yellow', 'black'];
const wolken_grenzen = [0, 5, 10, 15];
const temp_kleuren = ['#8080ff', 'black', 'yellow', 'orange', 'red', 'purple'];
const temp_grenzen = [-100, 0, 25, 30, 35, 40];
const gust_kleuren = ['black', 'yellow', 'orange', 'red'];
const gust_grenzen = [0, 41, 54, 65];
const gust_kmu_grenzen = [0, 75, 100, 120];
const druk_tendens_grenzen = [-9999,0,0.1];
const druk_tendens_kleuren = ['red', 'black', 'blue'];
const model_compare_grenzen = [-9999,0,0.05];
const model_compare_kleuren = ['blue', 'black', 'red'];
    
// LUT wawa codes descriptions
const wawaDscr = {
    4:   "Heiigheid of rook, of stof zwevend in de lucht, zicht ≥ 1 km",
    5:   "Heiigheid of rook, of stof zwevend in de lucht, zicht < 1 km",
    10:  "Nevel",
    11:  "IJsnaalden, ijsplaatjes",
    12:  "Weerlicht of bliksem op afstand",
    18:  "Langdurige, zware windstoten: 'squalls'",
    20:  "Mist",
    21:  "Neerslag in afgelopen uur",
    22:  "Motregen (niet onderkoeld) of motsneeuw in afgelopen uur",
    23:  "Regen (niet onderkoeld) in afgelopen uur",
    24:  "Sneeuw in afgelopen uur",
    25:  "Onderkoelde regen of motregen in afgelopen uur",
    26:  "Onweer (met of zonder neerslag) in afgelopen uur",
    27:  "Driftsneeuw of opwaaiend zand in afgelopen uur",
    28:  "Driftsneeuw of opwaaiend zand in afgelopen uur, zicht ≥ 1 km",
    29:  "Driftsneeuw of opwaaiend zand in afgelopen uur, zicht < 1 km",
    30:  "Mist",
    31:  "Mist of ijsmist in banken",
    32:  "Mist of ijsmist, dunner geworden gedurende afgelopen uur",
    33:  "Mist of ijsmist, geen merkbare verandering gedurende afgelopen uur",
    34:  "Mist of ijsmist, opgekomen of dikker geworden gedurende afgelopen uur",
    35:  "Mist met aanzetting van ruige rijp",
    40:  "Neerslag",
    41:  "Neerslag, licht of matig",
    42:  "Neerslag, zwaar",
    43:  "Vloeibare neerslag, licht of matig",
    44:  "Vloeibare neerslag, zwaar",
    45:  "Vaste neerslag, licht of matig",
    46:  "Vaste neerslag, zwaar",
    47:  "Onderkoelde neerslag, licht of matig",
    48:  "Onderkoelde neerslag, zwaar",
    50:  "Motregen",
    51:  "Motregen, niet onderkoeld, licht",
    52:  "Motregen, niet onderkoeld, matig",
    53:  "Motregen, niet onderkoeld, zwaar",
    54:  "Motregen, onderkoeld, licht",
    55:  "Motregen, onderkoeld, matig",
    56:  "Motregen, onderkoeld, zwaar",
    57:  "Motregen en regen, licht",
    58:  "Motregen en regen, matig of zwaar",
    60:  "Regen",
    61:  "Regen, niet onderkoeld, licht",
    62:  "Regen, niet onderkoeld, matig",
    63:  "Regen, niet onderkoeld, zwaar",
    64:  "Regen, onderkoeld, licht",
    65:  "Regen, onderkoeld, matig",
    66:  "Regen, onderkoeld, zwaar",
    67:  "Regen (of motregen) en sneeuw, licht",
    68:  "Regen (of motregen) en sneeuw, matig of zwaar",
    70:  "Sneeuw",
    71:  "Sneeuw, licht",
    72:  "Sneeuw, matig",
    73:  "Sneeuw, zwaar",
    74:  "IJsregen, licht",
    75:  "IJsregen, matig",
    76:  "IJsregen, zwaar",
    77:  "Motsneeuw",
    78:  "IJskristalletjes",
    80:  "Bui(en) of neerslag van tijd tot tijd",
    81:  "Regenbui(en) of regen van tijd tot tijd, licht",
    82:  "Regenbui(en) of regen van tijd tot tijd, matig",
    83:  "Regenbui(en) of regen van tijd tot tijd, zwaar",
    84:  "Regenbui(en), zeer hevig, (wolkbreuk(en)) of regen van tijd tot tijd, zeer zwaar",
    85:  "Sneeuwbui(en) of sneeuw van tijd tot tijd, licht",
    86:  "Sneeuwbui(en) of sneeuw van tijd tot tijd, matig",
    87:  "Sneeuwbui(en) of sneeuw van tijd tot tijd, zwaar",
    89:  "Hagel",
    90:  "Onweer",
    91:  "Onweer, licht of matig zonder neerslag",
    92:  "Onweer, licht of matig met regen- en/of sneeubuien",
    93:  "Onweer, licht of matig met hagel",
    94:  "Onweer, zwaar zonder neerslag",
    95:  "Onweer, zwaar met regen- en/of sneeuwbuien",
    96:  "Onweer, zwaar met hagel",
    99:  "Tornado"
};

// warnings
// colours
const bftcol = new Array(
    "rgb(255,255,255)",
    "rgb(255,255,179)",
    "rgb(255,255,179)", 
    "rgb(255,255,0)", 
    "rgb(128,255,0)", 
    "rgb(0,140,48)",
    "rgb(0,255,255)",
    "rgb(115,163,255)",
    "rgb(0,0,255)",
    "rgb(255,51,255)",
    "rgb(255,128,0)",
    "rgb(255,0,0)",
    "rgb(0,0,0)"
    );
    
const neerslagcol = function(val){
    
    if (val==0) return "rgb(255,255,255)";
    if (val<1) return "rgb(210,210,210)";
    if (val<5) return "rgb(160,160,160)";
    if (val<10) return "rgb(255,128,128)";
    if (val<30) return "rgb(255,0,0)";
    if (val>=30) return "rgb(0,0,0)"; 
    
    return "rgb(255,255,255)"; 
}  

const cloudcol = function(val){
    
    if (val<100) return "rgb(229,55,50)";
    if (val<200) return "rgb(255,153,191)";
    if (val<350) return "rgb(255,255,25)";
    if (val<500) return "rgb(255,255,183)";
    if (val<1000) return "rgb(255,127,0)";
    if (val<1500) return "rgb(255,191,127)";
    if (val<2000) return "rgb(50,255,0)";
    if (val<3000) return "rgb(178,255,140)";
    if (val<5000) return "rgb(101,76,255)";

    return "rgb(255,255,255)";
}

const zichtcol = function(val){
    
    if (val<20) return "rgb(255,0,0)";
    if (val<50) return "rgb(255,127,0)";
    if (val<100) return "rgb(255,255,0)";
    if (val<200) return "rgb(127,255,0)";
    if (val<400) return "rgb(0,255,0)";
    if (val<700) return "rgb(0,255,127)";
    if (val<1000) return "rgb(0,255,255)";
    if (val<1500) return "rgb(0,127,255)";
    if (val<2500) return "rgb(0,0,255)";
    if (val<5000) return "rgb(100,0,255)";

    return "rgb(255,255,255)"; 
}  
// conversion constants
const knot = 0.514444444;
const kmh = 3.6;
