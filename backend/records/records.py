import pandas as pd
import json

stns = [215, 235, 240, 242, 249, 251, 260, 267, 269, 270, 273, 
    275, 277, 278, 279, 280, 283, 286, 290, 310, 319, 323, 330, 340,
    344, 348, 350, 356, 370, 375, 377, 380, 391]
    
mths = ['januari', 'februari', 'maart', 'april', 'mei', 'juni',
    'juli', 'augustus', 'september', 'oktober', 'november', 'december']
    
txx = {}
txn = {}
tnn = {}
tnx = {}
    
for s in stns:
    bestand = "data"+str(s)+".csv"
    df = pd.read_csv(bestand, header=9, skipinitialspace=True)
    txx[s] = []
    txn[s] = []
    tnn[s] = []
    tnx[s] = []
        
    winterxx = []
    winterxn = []
    winternx = []
    winternn = []
    lentexx = []
    lentexn = []
    lentenx = []
    lentenn = []
    zomerxx = []
    zomerxn = []
    zomernx = []
    zomernn = []
    herfstxx = []
    herfstxn = []
    herfstnx = []
    herfstnn = []
    
    # DAGEN
    dagen = df['YYYYMMDD'].astype(str).str[4:8]
    df['MMDD']=dagen
    txx[s] = list(df.groupby('MMDD')['TX'].max().values/10)
    txn[s] = list(df.groupby('MMDD')['TX'].min().values/10)
    tnx[s] = list(df.groupby('MMDD')['TN'].max().values/10)
    tnn[s] = list(df.groupby('MMDD')['TN'].min().values/10)
    
    # DECADES
    for m in range(1, 13):
        maand = df['YYYYMMDD'].astype(str).str[4:6] == str(m).zfill(2)
        
        dagen = df[maand]['YYYYMMDD'].astype(str).str[6:8]
        decade1 = dagen.astype(int) < 11
        decade2a = dagen.astype(int) > 10
        decade2b = dagen.astype(int) < 21
        decade3 = dagen.astype(int) > 20
            
        # hoogste en laagste maxima per decade ooit
        txx[s].append(df[maand & decade1]['TX'].max()/10)
        txx[s].append(df[maand & decade2a & decade2b]['TX'].max()/10)
        txx[s].append(df[maand & decade3]['TX'].max()/10)
        txn[s].append(df[maand & decade1]['TX'].min()/10)
        txn[s].append(df[maand & decade2a & decade2b]['TX'].min()/10)
        txn[s].append(df[maand & decade3]['TX'].min()/10)
        
        # hoogste en laagste minima per decade ooit
        tnx[s].append(df[maand & decade1]['TN'].max()/10)
        tnx[s].append(df[maand & decade2a & decade2b]['TN'].max()/10)
        tnx[s].append(df[maand & decade3]['TN'].max()/10)
        tnn[s].append(df[maand & decade1]['TN'].min()/10)
        tnn[s].append(df[maand & decade2a & decade2b]['TN'].min()/10)
        tnn[s].append(df[maand & decade3]['TN'].min()/10)
        
    # MAANDEN
    for m in range(1, 13):
        maand = df['YYYYMMDD'].astype(str).str[4:6] == str(m).zfill(2)
        
        # hoogste en laagste maxima per maand ooit
        txx[s].append(df[maand]['TX'].max()/10)
        txn[s].append(df[maand]['TX'].min()/10)
        
        # hoogste en laagste minima per maand ooit
        tnn[s].append(df[maand]['TN'].min()/10)
        tnx[s].append(df[maand]['TN'].max()/10)
        
    # SEIZOENEN
    for m in range(1, 13):
        maand = df['YYYYMMDD'].astype(str).str[4:6] == str(m).zfill(2)
        
        if m in (1, 2, 12):
            winterxx.append(df[maand]['TX'].max())
            winterxn.append(df[maand]['TX'].min())
            winternx.append(df[maand]['TN'].max())
            winternn.append(df[maand]['TN'].min())
        if m in (3, 4, 5):
            lentexx.append(df[maand]['TX'].max())
            lentexn.append(df[maand]['TX'].min())
            lentenx.append(df[maand]['TN'].max())
            lentenn.append(df[maand]['TN'].min())
        if m in (6, 7, 8):
            zomerxx.append(df[maand]['TX'].max())
            zomerxn.append(df[maand]['TX'].min())
            zomernx.append(df[maand]['TN'].max())
            zomernn.append(df[maand]['TN'].min())
        if m in (9, 10, 11):
            herfstxx.append(df[maand]['TX'].max())
            herfstxn.append(df[maand]['TX'].min())
            herfstnx.append(df[maand]['TN'].max())
            herfstnn.append(df[maand]['TN'].min())
        
    # hoogste en laagste maxima per seizoen ooit
    txx[s].append(max(winterxx)/10) 
    txx[s].append(max(lentexx)/10) 
    txx[s].append(max(zomerxx)/10) 
    txx[s].append(max(herfstxx)/10)
    
    txn[s].append(min(winterxn)/10) 
    txn[s].append(min(lentexn)/10) 
    txn[s].append(min(zomerxn)/10) 
    txn[s].append(min(herfstxn)/10)
    
    # hoogste en laagste minima per seizoen ooit
    tnx[s].append(max(winternx)/10) 
    tnx[s].append(max(lentenx)/10) 
    tnx[s].append(max(zomernx)/10) 
    tnx[s].append(max(herfstnx)/10)
    
    tnn[s].append(min(winternn)/10) 
    tnn[s].append(min(lentenn)/10) 
    tnn[s].append(min(zomernn)/10) 
    tnn[s].append(min(herfstnn)/10)
    
    # JAAR        
    # hoogste en laagste maxima ooit
    txx[s].append(df['TX'].max()/10);
    txn[s].append(df['TX'].min()/10);
    
    # hoogste en laagste minima ooit
    tnx[s].append(df['TN'].max()/10);
    tnn[s].append(df['TN'].min()/10);

# opslaan als json    
out = open("txx.json", "w")    
json.dump(txx, out)
out.close()

out = open("tnx.json", "w") 
json.dump(tnx, out)
out.close()

out = open("txn.json", "w") 
json.dump(txn, out)
out.close()

out = open("tnn.json", "w") 
json.dump(tnn, out)
out.close()

    
    
    
    
    
