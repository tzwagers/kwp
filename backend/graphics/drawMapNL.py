import numpy as np
import math

# matplotlib
import matplotlib
from mpl_toolkits.basemap import Basemap
import matplotlib.pyplot as plt
from matplotlib.colors import from_levels_and_colors
from matplotlib.offsetbox import OffsetImage, AnnotationBbox
import matplotlib.patheffects as path_effects
from matplotlib.patches import Polygon
from matplotlib.collections import PatchCollection
from matplotlib.patches import PathPatch
from matplotlib import cm
import matplotlib.ticker as ticker

# Use PIL to crop image
from PIL import Image, ImageChops

# EHAA 
from extrageoNL import *

# colors ###############################################################
water = '#202030'
edge = '#202020'
nlcol = '#203020'
contcol = '#303030'
fircol = '#8080c0'

# provincies ###########################################################
provs = ('Groningen', 'Friesland', 'Drenthe', 'Overijssel',\
 'Flevoland', 'Noord-Holland', 'Zuid-Holland', 'Utrecht',\
  'Gelderland', 'Limburg', 'Noord-Brabant', 'Zeeland')

# domain ###############################################################
lonn = 0.0
lonx = 7.5
latn = 50.0
latx = 55.8

# consts ###############################################################
output = 'mapNL.svg'

# setup map ############################################################
fig = plt.figure(figsize=(1010/80, 1600/80), dpi=80, num=1)
ax1 =  plt.subplot2grid((1,1), (0,0))
plt.tight_layout()

m = Basemap(resolution='h', projection='merc',\
			llcrnrlon=lonn, llcrnrlat=latn,\
			urcrnrlon=lonx, urcrnrlat=latx, ax=ax1)
			
m.drawcoastlines(color=edge, linewidth=0)
m.drawcountries(color=edge, linewidth=0)
m.drawmapboundary(fill_color=water)
m.drawrivers(color=water, linewidth=2, zorder=6)

# getting all polygons used to draw the coastlines of the map
# en kleur dan het land in. Dit is momenteel de enige manier nu
# fillcontinents en drawlsmask niet meer werken
for p in m.landpolygons:
	poly = Polygon(p.boundary, facecolor=contcol)
	plt.gca().add_patch(poly)

# draw Veluwemeer en Zeeuwse wateren ###################################
wpatches = []
m.readshapefile('NLD_adm/NLD_adm1','waterbody', drawbounds=False)
for info, shape in zip(m.waterbody_info, m.waterbody):
	wpatches.append(Polygon(np.array(shape), True))
ax1.add_collection(PatchCollection(wpatches, facecolor=water, linewidths=0, zorder=2))

# draw provincies ######################################################
ppatches = []
m.readshapefile('provinciegrenzen_2019/2019_provinciegrenzen_kustlijn','provincies', drawbounds=False)
for info, shape in zip(m.provincies_info, m.provincies):
	if info['provincien'] in provs:
		ppatches.append(Polygon(np.array(shape), True))
ax1.add_collection(PatchCollection(ppatches, facecolor= nlcol, edgecolor=edge, linewidths=0.5, zorder=4))

# draw Zeeuwse meren ###################################################
wpatches = []
for info, shape in zip(m.waterbody_info, m.waterbody):
	if info['NAME_1']=='Zeeuwse meren':
		wpatches.append(Polygon(np.array(shape), True))
ax1.add_collection(PatchCollection(wpatches, facecolor=water, linewidths=0, zorder=5))

# Tiengemeten, Hompelvoet en Zeeuwse eilanden/platen ###################
ppatches = []
for info, shape in zip(m.provincies_info, m.provincies):
	if info['provincien'] in provs:
		if info['provincien']=='Zuid-Holland' and info['RINGNUM']>=3:
			ppatches.append(Polygon(np.array(shape), True))
		if info['provincien']=='Zeeland' and info['RINGNUM']>=5:
			ppatches.append(Polygon(np.array(shape), True))
ax1.add_collection(PatchCollection(ppatches, facecolor= nlcol, edgecolor=edge, linewidths=0.5, zorder=5))

# draw lakes ###########################################################
patches = []
m.readshapefile('NLD_adm/NLD_water_areas_dcw','waters', drawbounds=False)
for info, shape in zip(m.waters_info, m.waters):
	patches.append(Polygon(np.array(shape), True))
ax1.add_collection(PatchCollection(patches, facecolor=water, edgecolor=edge, linewidths=0.5, zorder=5))

# Noordzee FIR bdry ####################################################
lines = EHAA_NorthSeaAreaV()
for l in lines:
	xpoints = []
	ypoints = []
	for seg in l:
		x, y = m(seg[0], seg[1])
		xpoints.append(x)
		ypoints.append(y)
	plt.plot(xpoints, ypoints, color=fircol, linewidth=0.5, zorder=3)

# wegschrijven van de plot #############################################
# + verwijderen zwarte rand
plt.subplots_adjust(top = 1, bottom = 0, right = 1, left = 0, 
            hspace = 0, wspace = 0)
plt.margins(0,0)
plt.gca().xaxis.set_major_locator(ticker.NullLocator())
plt.gca().yaxis.set_major_locator(ticker.NullLocator())
ax1.xaxis.set_ticks([])
ax1.yaxis.set_ticks([])
for spine in ax1.spines.items():
    spine[1].set_visible(False)
plt.tight_layout()
plt.savefig(output, bbox_inches='tight', pad_inches=-0.1)
