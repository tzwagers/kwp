 #!/usr/bin/python3

import numpy as np
import pygrib
import math
import datetime
import sys
import os
import re
import glob
import shutil
import json
from scipy.interpolate import RegularGridInterpolator

# vars
homedir = '/usr/people/zwagers/kwp/'
datadir = '/usr/people/zwagers/wsdata/data/gvdb/ecmwf/'
outdir = '/usr/people/zwagers/public_html/kwp/'
laatste = 'lastrun.txt'
stations = 'stations_ned.json'
EC_prefix = 'ECMD_EUR_'
EC_postfix = '_GB'
minH = 0
maxH = 36
step = 1
ECts = dict()
ECrun = dict()

# const
C = 273.15
KT = 1.94384
KMH = 3.6

# return date/time object
def dag(run, t):
    out = datetime.datetime(int(run[0:4]), int(run[4:6]), int(run[6:8]), int(run[8:10]), int(run[10:12]))
    return out + datetime.timedelta(hours=t)
    
# retrieve parameter
def getParameter(gr, par, tijd, run):
    number_of_stations=len(data)
    msg = gr.select(shortName=par)
    
    # lats van laag naar hoog om te kunnen interpoleren
    lats1D=[]
    lats, lons = msg[0].latlons()
    for i in lats:
        lats1D.append(i[0])
    
    for i in data:
        # ook volgorde matrixwaardes aanpassen!!
        itp = RegularGridInterpolator((lats1D[::-1], lons[0]), msg[0].values[::-1], method='linear')
        ECrun[tijd] = run
        try: 
            ECts[i][par][tijd] = itp((data[i][0], data[i][1])) - C 
        except:
            try:
                ECts[i][par] = dict()
                ECts[i][par][tijd] = itp((data[i][0], data[i][1])) - C
            except:
                ECts[i] = dict()
                ECts[i][par] = dict()
                ECts[i][par][tijd] = itp((data[i][0], data[i][1])) - C
    
# laatste 4 runs?
runs = sorted(list(glob.iglob(datadir+EC_prefix+'*'+str(maxH*100)+EC_postfix)))

# stationsdata laden
f = open (homedir+stations, "r")
data = json.loads(f.read())
f.close()

# parameters
par = ["2t", "2d"];

for p in par:
    for r in runs[-4:]:
        run = re.findall("\d{12}", r)[0]

        # data binnenhalen
        for t in range(minH, maxH+1, step):
            fcst = dag(run, t)
            hour = "%05d" % (t*100)
            file = datadir+EC_prefix+run+'_'+hour+EC_postfix
            gr = pygrib.open(file)
            #print(file)
            getParameter(gr, p, fcst.strftime("%Y%m%d%H%M"), run)
        
# interpoleren naar 10 minuten-waarden
stations = list(ECts)
for p in par:
    for stn in stations:
        t = [0]*7
        times = sorted(list(ECts[stn][p]))
        
        for i in range(0, len(times)-1):
            t[0] = times[i]
            t[6] = times[i+1]
            arr = np.linspace(ECts[stn][p][t[0]], ECts[stn][p][t[6]], 7)        
            
            for j in range(1, 6):
                t[j] = t[0][0:10] + str(j*10)
                ECts[stn][p][t[j]] = arr[j]
                ECrun[t[j]] = ECrun[t[0]]

with open (outdir+"EC_ts.json", "w") as outfile:
    json.dump(ECts, outfile)
    
with open (outdir+"EC_run.json", "w") as outfile:
    json.dump(ECrun, outfile)
