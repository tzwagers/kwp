""" Create JSON-file with observational data

This script reads all files which contain 10 minute observation data
in csv-format. The structure of the csv should be as follows:

no headers
column 0: sensor ID or synop number
column 1: date as YYYYMMDD
column 2: time as HHII
column 3: '10 Min' (unused)
column 4: parameter
column 5: value
column 6: second value (unused)
more columns optional

The data will be converted into a dict, which will be saved as
a json-file to the path defined by the variable 'frontend_dir'

This script only requires modules from the standard library that is
distributed with Python 3.5+

Thom Zwagers, KNMI, 2021-07-01
"""

import csv
import glob
import json
import re
import yaml
from collections import defaultdict

class RecursiveDefaultdict(defaultdict):
    """ A class used to create multi-dimensional defaultdics
    
    Whenever an extra dimension is required, a new defaultdict will be
    created. Using this, dicts can be extended on the fly
    
    Taken from http://kentsjohnson.com/kk/00013.html 
    """
    def __init__(self): 
        self.default_factory = type(self)
        
def main():
    with open("config.yml", 'r') as ymlfile:
        cfg = yaml.safe_load(ymlfile)
        
    backend_dir = cfg['backend_dir']
    frontend_nl_dir = cfg['frontend_nl_dir']
    nl_datadir = cfg['nl_datadir']
    nl_datafmt = cfg['nl_datafmt']
    jsonfile = cfg['jsonfile']
    latest_file = cfg['latest_file']
    
    # create list of all datafiles
    datafiles = glob.glob(nl_datadir+nl_datafmt)
    datafiles.sort()
    latest_obs = re.findall("[0-9]{12}", datafiles[-1])
    datafiles.extend(glob.glob(be_datadir+be_datafmt))
    
    # read all files and put values in dictionary
    out_bes = RecursiveDefaultdict()
    out_nl = RecursiveDefaultdict()
    for f in datafiles:
        with open(f) as csv_file:
            for r in csv.reader(csv_file):
                r = [x.strip() for x in r] # remove whitespaces
                if len(r)>=7: # there must be enough items (7+)
                    try:
                        out_nl[r[1]+r[2]][r[0]][r[4]] = r[5]
                    except:
                        pass # don't add data in case of an error
            
    # save dictionary as json-file to nl-frontend directory TO DO
    with open(frontend_nl_dir+jsonfile, 'w') as output_file:
        json.dump(out_nl, output_file) 
        
    # save latest timestamp to file (nl-frontend) TO DO
    fp = open(frontend_nl_dir+latest_file, 'w')
    fp.write(latest_obs[0])
    fp.close()
    
if __name__ == "__main__":
    main()
